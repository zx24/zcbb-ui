# Requirements 
- Vue3
- Element Plus

# Usage

```shell
# 1. 安装
yarn add zcbb-ui 
```

```js
// 2.引入 main.ts 
import ElementPlus from 'element-plus' // Element Plus
import 'element-plus/dist/index.css'
import ZcbbUi from 'zcbb-ui' // ZcbbUi
import 'zcbb-ui/dist/style.css'
createApp(App).use(ElementPlus).use(ZcbbUi).mount('#app')
```



```html
<!-- 3. 使用 ZbBottomSection 组件 -->
<template>
    <h1>表单底部栏</h1>
    <ZbBottomSection>
      <el-button type="primary" style="margin-left: 10px;">提交</el-button>
      <el-button>取消</el-button>
    </ZbBottomSection>
</template>
```


```html
<!-- 3. 使用 ZbButton 组件 -->
<script setup lang="ts">
import { ref } from 'vue'
const count = ref(0)
const handleClick = () => {s
    count.value++
}
</script>
<template>
     <h1>防抖按钮</h1>
    <ZbButton type="primary" :debounceWait="500" @click="handleClick">
        提交 
    </ZbButton>
    <div>点击{{ count }}次</div>
</template>
```


