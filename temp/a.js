// filename: vite.config.ts
import { defineConfig } from 'vite'
import { resolve } from "path";
import vue from '@vitejs/plugin-vue'

export default defineConfig({
  plugins: [
    vue(), 
    // vueJsx({})
  ],
  build: {
    // rollup打包配置
    rollupOptions: {
      // 请确保外部化那些你的库中不需要的依赖
      external: ['vue'],
      output: {
        // format: 'es', // 默认es，可选 'amd' 'cjs' 'es' 'iife' 'umd' 'system'
        // 在 UMD 构建模式下为这些外部化的依赖提供一个全局变量
        globals: {
          vue: 'Vue',
        },
      },
    },
    // 库模式
    lib: {
      name: 'FbUi',
      entry: resolve(__dirname, "src/packages/index.ts"), // 打包文件入口
      fileName: 'fb-ui' // 打包文件的名字
    },
  },
})
