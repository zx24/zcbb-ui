import { buttonProps } from 'element-plus'

import type { ExtractPropTypes, PropType } from 'vue'
import type { DebounceSettings, ThrottleSettings } from 'lodash-es'

export const ZbButtonProps = {
  ...buttonProps,
  throttleWait: {
    type: Number
  },
  throttleSetting: {
    type: Object as PropType<ThrottleSettings>,
    default: () => ({ leading: true } as ThrottleSettings)
  },
  debounceWait: {
    type: Number
  },
  debounceSetting: {
    type: Object as PropType<DebounceSettings>
  }
}

// ExtractPropTypes：接受一个类型，返回vue3处理后的类型 https://cn.vuejs.org/api/utility-types.html#extractproptypes
// buttonProps: node_modules/element-plus/es/components/button/src/button.d.ts
// 目标: 在el-button类型props基础上，新增自己的props ts定义
export type ZbButtonProps = ExtractPropTypes<typeof ZbButtonProps>