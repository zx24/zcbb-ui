import type { Meta, StoryObj } from '@storybook/vue3';
import ZbBottomSection from '../packages/BottomSection/index.vue';

const meta = {
  title: 'Example/ZbBottomSection',
  component: ZbBottomSection,
  tags: ['autodocs'],
} satisfies Meta<typeof ZbBottomSection>;

export default meta;
type Story = StoryObj<typeof meta>;

export const slot: Story = {
  name: '默认slot', // name可修改 =》 左侧菜单
  render: () => ({
    components: { ZbBottomSection },
    template: '<ZbBottomSection><button style="margin-left: 10px;">提交</button><button>取消</button></ZbBottomSection>',
  }),
};
