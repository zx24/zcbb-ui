import type { Meta, StoryObj } from '@storybook/vue3';

import ZbButton from '../packages/Button/index.vue';

const meta = {
  title: 'Example/ZbButton',
  component: ZbButton,
  tags: ['autodocs'],
  argTypes: {
    // attr
    type:{
      control: 'text',
      description: '同element type',
    },
    throttleWait: {
      control: 'number',
      description: '节流时间',
    },
    throttleSetting:{
      control: 'object',
      description: 'lodash节流选项',
    },
    debounceWait:{
      control: 'number',
      description: '防抖延迟时间',
    },
    debounceSetting:{
      control: 'object',
      description: 'lodash防抖选项',
    },
    // stot 
    default:{
      description: '按钮默认文字',
      control: 'text'
    }
  }
} satisfies Meta<typeof ZbButton>;

export default meta;
type Story = StoryObj<typeof meta>;

export const 节流按钮: Story = {
  render: (args) => ({
    components: { ZbButton },
    setup() {
      return { args };
    },
    template: '<ZbButton v-bind="args">节流按钮500ms</ZbButton>',
  }),
  args: {
    type: 'primary',
    throttleWait: 500
  },
};


export const 防抖按钮: Story = {
  render: (args) => ({
    components: { ZbButton },
    setup() {
      return { args };
    },
    template: '<ZbButton v-bind="args">防抖按钮500ms</ZbButton>',
  }),
  args: {
    type: 'danger',
    debounceWait: 500
  },
};
