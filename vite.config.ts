import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers'
import { resolve } from "path";
import { visualizer } from 'rollup-plugin-visualizer';
import dts from 'vite-plugin-dts'

export default defineConfig({
  plugins: [
    vue(),
    visualizer(),
    AutoImport({
      imports: ["vue", "vue-router"],
      resolvers: [ElementPlusResolver()],
    }),
    Components({
      resolvers: [ElementPlusResolver()],
    }),
    // 如果你希望将所有的类型合并到一个文件中，只需指定 rollupTypes: true：
    dts({rollupTypes: true }),
  ],
  build: {
    rollupOptions: {
      external: ["vue", "element-plus"],
      output: {
        format: "es",
        exports: "named",
        globals: {vue: 'Vue', "element-plus": "elementPlus"}
      },
    },
    lib: {
      name: 'ZcbbUi',
      entry: resolve(__dirname, "src/packages/index.ts"), 
      fileName: 'zcbb-ui' 
    },
  },
})
