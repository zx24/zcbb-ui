import { AllowedComponentProps } from 'vue';
import { Component as Component_2 } from 'vue';
import { ComponentCustomProps } from 'vue';
import { ComponentOptionsMixin } from 'vue';
import { ComputedOptions } from 'vue';
import { DebounceSettings } from 'lodash';
import { DefineComponent } from 'vue';
import { EpPropFinalized } from 'element-plus/es/utils/index.mjs';
import { EpPropMergeType } from 'element-plus/es/utils/index.mjs';
import { ExtractPropTypes as ExtractPropTypes_2 } from 'vue';
import { MethodOptions } from 'vue';
import { PropType as PropType_2 } from 'vue';
import { ThrottleSettings } from 'lodash';
import { VNodeProps } from 'vue';

declare type __VLS_WithTemplateSlots<T, S> = T & {
    new (): {
        $slots: S;
    };
};

declare type __VLS_WithTemplateSlots_2<T, S> = T & {
    new (): {
        $slots: S;
    };
};

export declare const ZbBottomSection: __VLS_WithTemplateSlots_2<DefineComponent<{}, {}, {}, {}, {}, ComponentOptionsMixin, ComponentOptionsMixin, {}, string, VNodeProps & AllowedComponentProps & ComponentCustomProps, Readonly<ExtractPropTypes_2<{}>>, {}, {}>, {
    default?(_: {}): any;
}>;

export declare const ZbButton: __VLS_WithTemplateSlots<DefineComponent<{
    throttleWait: {
        type: NumberConstructor;
    };
    throttleSetting: {
        type: PropType_2<ThrottleSettings>;
        default: () => ThrottleSettings;
    };
    debounceWait: {
        type: NumberConstructor;
    };
    debounceSetting: {
        type: PropType_2<DebounceSettings>;
    };
    size: {
        readonly type: PropType_2<EpPropMergeType<StringConstructor, "" | "default" | "small" | "large", never>>;
        readonly required: false;
        readonly validator: ((val: unknown) => boolean) | undefined;
        __epPropKey: true;
    };
    disabled: BooleanConstructor;
    type: EpPropFinalized<StringConstructor, "" | "default" | "success" | "warning" | "info" | "text" | "primary" | "danger", unknown, "", boolean>;
    icon: {
        readonly type: PropType_2<EpPropMergeType<(new (...args: any[]) => (string | Component_2<any, any, any, ComputedOptions, MethodOptions>) & {}) | (() => string | Component_2<any, any, any, ComputedOptions, MethodOptions>) | ((new (...args: any[]) => (string | Component_2<any, any, any, ComputedOptions, MethodOptions>) & {}) | (() => string | Component_2<any, any, any, ComputedOptions, MethodOptions>))[], unknown, unknown>>;
        readonly required: false;
        readonly validator: ((val: unknown) => boolean) | undefined;
        __epPropKey: true;
    };
    nativeType: EpPropFinalized<StringConstructor, "button" | "reset" | "submit", unknown, "button", boolean>;
    loading: BooleanConstructor;
    loadingIcon: EpPropFinalized<(new (...args: any[]) => (string | Component_2<any, any, any, ComputedOptions, MethodOptions>) & {}) | (() => string | Component_2<any, any, any, ComputedOptions, MethodOptions>) | ((new (...args: any[]) => (string | Component_2<any, any, any, ComputedOptions, MethodOptions>) & {}) | (() => string | Component_2<any, any, any, ComputedOptions, MethodOptions>))[], unknown, unknown, () => DefineComponent<{}, {}, {}, ComputedOptions, MethodOptions, ComponentOptionsMixin, ComponentOptionsMixin, {}, string, VNodeProps & AllowedComponentProps & ComponentCustomProps, Readonly<ExtractPropTypes_2<{}>>, {}, {}>, boolean>;
    plain: BooleanConstructor;
    text: BooleanConstructor;
    link: BooleanConstructor;
    bg: BooleanConstructor;
    autofocus: BooleanConstructor;
    round: BooleanConstructor;
    circle: BooleanConstructor;
    color: StringConstructor;
    dark: BooleanConstructor;
    autoInsertSpace: EpPropFinalized<BooleanConstructor, unknown, unknown, undefined, boolean>;
    tag: EpPropFinalized<(new (...args: any[]) => (string | Component_2<any, any, any, ComputedOptions, MethodOptions>) & {}) | (() => string | Component_2<any, any, any, ComputedOptions, MethodOptions>) | ((new (...args: any[]) => (string | Component_2<any, any, any, ComputedOptions, MethodOptions>) & {}) | (() => string | Component_2<any, any, any, ComputedOptions, MethodOptions>))[], unknown, unknown, "button", boolean>;
}, {}, unknown, {}, {}, ComponentOptionsMixin, ComponentOptionsMixin, {
    click: (val: MouseEvent) => void;
}, string, VNodeProps & AllowedComponentProps & ComponentCustomProps, Readonly<ExtractPropTypes_2<{
    throttleWait: {
        type: NumberConstructor;
    };
    throttleSetting: {
        type: PropType_2<ThrottleSettings>;
        default: () => ThrottleSettings;
    };
    debounceWait: {
        type: NumberConstructor;
    };
    debounceSetting: {
        type: PropType_2<DebounceSettings>;
    };
    size: {
        readonly type: PropType_2<EpPropMergeType<StringConstructor, "" | "default" | "small" | "large", never>>;
        readonly required: false;
        readonly validator: ((val: unknown) => boolean) | undefined;
        __epPropKey: true;
    };
    disabled: BooleanConstructor;
    type: EpPropFinalized<StringConstructor, "" | "default" | "success" | "warning" | "info" | "text" | "primary" | "danger", unknown, "", boolean>;
    icon: {
        readonly type: PropType_2<EpPropMergeType<(new (...args: any[]) => (string | Component_2<any, any, any, ComputedOptions, MethodOptions>) & {}) | (() => string | Component_2<any, any, any, ComputedOptions, MethodOptions>) | ((new (...args: any[]) => (string | Component_2<any, any, any, ComputedOptions, MethodOptions>) & {}) | (() => string | Component_2<any, any, any, ComputedOptions, MethodOptions>))[], unknown, unknown>>;
        readonly required: false;
        readonly validator: ((val: unknown) => boolean) | undefined;
        __epPropKey: true;
    };
    nativeType: EpPropFinalized<StringConstructor, "button" | "reset" | "submit", unknown, "button", boolean>;
    loading: BooleanConstructor;
    loadingIcon: EpPropFinalized<(new (...args: any[]) => (string | Component_2<any, any, any, ComputedOptions, MethodOptions>) & {}) | (() => string | Component_2<any, any, any, ComputedOptions, MethodOptions>) | ((new (...args: any[]) => (string | Component_2<any, any, any, ComputedOptions, MethodOptions>) & {}) | (() => string | Component_2<any, any, any, ComputedOptions, MethodOptions>))[], unknown, unknown, () => DefineComponent<{}, {}, {}, ComputedOptions, MethodOptions, ComponentOptionsMixin, ComponentOptionsMixin, {}, string, VNodeProps & AllowedComponentProps & ComponentCustomProps, Readonly<ExtractPropTypes_2<{}>>, {}, {}>, boolean>;
    plain: BooleanConstructor;
    text: BooleanConstructor;
    link: BooleanConstructor;
    bg: BooleanConstructor;
    autofocus: BooleanConstructor;
    round: BooleanConstructor;
    circle: BooleanConstructor;
    color: StringConstructor;
    dark: BooleanConstructor;
    autoInsertSpace: EpPropFinalized<BooleanConstructor, unknown, unknown, undefined, boolean>;
    tag: EpPropFinalized<(new (...args: any[]) => (string | Component_2<any, any, any, ComputedOptions, MethodOptions>) & {}) | (() => string | Component_2<any, any, any, ComputedOptions, MethodOptions>) | ((new (...args: any[]) => (string | Component_2<any, any, any, ComputedOptions, MethodOptions>) & {}) | (() => string | Component_2<any, any, any, ComputedOptions, MethodOptions>))[], unknown, unknown, "button", boolean>;
}>> & {
    onClick?: ((val: MouseEvent) => any) | undefined;
}, {
    text: boolean;
    type: EpPropMergeType<StringConstructor, "" | "default" | "success" | "warning" | "info" | "text" | "primary" | "danger", unknown>;
    throttleSetting: ThrottleSettings;
    disabled: boolean;
    nativeType: EpPropMergeType<StringConstructor, "button" | "reset" | "submit", unknown>;
    loading: boolean;
    loadingIcon: EpPropMergeType<(new (...args: any[]) => (string | Component_2<any, any, any, ComputedOptions, MethodOptions>) & {}) | (() => string | Component_2<any, any, any, ComputedOptions, MethodOptions>) | ((new (...args: any[]) => (string | Component_2<any, any, any, ComputedOptions, MethodOptions>) & {}) | (() => string | Component_2<any, any, any, ComputedOptions, MethodOptions>))[], unknown, unknown>;
    plain: boolean;
    link: boolean;
    bg: boolean;
    autofocus: boolean;
    round: boolean;
    circle: boolean;
    dark: boolean;
    autoInsertSpace: EpPropMergeType<BooleanConstructor, unknown, unknown>;
    tag: EpPropMergeType<(new (...args: any[]) => (string | Component_2<any, any, any, ComputedOptions, MethodOptions>) & {}) | (() => string | Component_2<any, any, any, ComputedOptions, MethodOptions>) | ((new (...args: any[]) => (string | Component_2<any, any, any, ComputedOptions, MethodOptions>) & {}) | (() => string | Component_2<any, any, any, ComputedOptions, MethodOptions>))[], unknown, unknown>;
}, {}>, {
    default?(_: {}): any;
    loading?(_: {}): any;
    icon?(_: {}): any;
}>;

export { }
